package Ferzle;

/**
 * @author Charles Cusack
 * @version 1.0, September, 2006
 * Modified, February, 2008
 * 
 * The class which represents events in the Ferzle Model
 * Since the model is fairly simple, the only detail that is 
 * needed is the source of the event.
 *
 */
import java.util.EventObject;

public class FerzleEvent extends EventObject {
    /** 
     * The constructor
     * @param src the object that caused the creation of the event
     */
    public FerzleEvent(Object src) {
        super(src);
    }
}
