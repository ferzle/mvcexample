package Ferzle;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import javax.swing.JFrame;

/**
 * @author Charles Cusack
 * @version 1.0, September, 2006
 * 
 */
public class MVC_Ferzle  {
    
    public MVC_Ferzle() {
    	// Instantiate the model, view, and controller.
        FerzleModel model=new FerzleModel();
        FerzleView view=new FerzleView(model);
        new FerzleController(model,view);   
        
        // Create the frame and put the view on it.
    	JFrame theFrame = new JFrame();
        theFrame.setTitle("A useless application");
        theFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                
        Container cont=theFrame.getContentPane();
        cont.add(view,BorderLayout.CENTER);
  
        // Finish setting up the main window
        theFrame.setBackground(Color.white);
        theFrame.pack(); 
        theFrame.setSize(new Dimension(300,200));
        theFrame.setVisible(true);   
    }    
    public static void main(String[] args) {  
    	new MVC_Ferzle();
     }    
}
