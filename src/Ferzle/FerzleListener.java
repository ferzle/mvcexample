package Ferzle;

/**
 * @author Charles Cusack
 * @version 1.0, September 2006
 * Modified February, 2008
 * 
 * The listener interface for the Ferzle Model
 * The interface contains only one method, since the model is very simple.
 */
import java.util.EventListener;

public interface FerzleListener extends EventListener {
    /*
     * The overridden method will tell what to do if the data from the 
     * model has changed
     *
     * @param e the event
     */
    public void ferzleDataChanged(FerzleEvent e);
}
