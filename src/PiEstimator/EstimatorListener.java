package PiEstimator;

/**
 * @author Charles Cusack
 * @version 1.0, Octber 5, 2002
 * 
 * The listener interface for the Estimator Model
 * The interface contains only one method, since the model is very simple.
 */
public interface EstimatorListener {
    /*
     * The overridden method will tell what to do if the data from the 
     * model has changed
     * @param e the event
     */
    public void estimatorDataChanged(EstimatorEvent e);
    
    // If the model was a little more complicated, we might also want
    // to include the following methods in the interface.
    // In fact, the way the model works right now, the model ends up
    // changing the entire model, even if only one of the parameters
    // changes.
    //public void estimateIntervalChanged(EstimatorEvent e);
    //public void firstEstimateChanged(EstimatorEvent e);
    //public void numberOfEstimatesChanged(EstimatorEvent e);
}
