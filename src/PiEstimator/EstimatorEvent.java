package PiEstimator;

/**
 * @author Charles Cusack
 * @version 1.0, Octber 5, 2002
 * 
 * The class which represents events in the Estimator Model
 * Since the model is fairly simple, the only detail that is 
 * needed is the source of the event.
 *
 */
public class EstimatorEvent {
    
    private Object source;  // The source of the event
    
    /** 
     * The constructor
     * @param src the object that caused the creation of the event
     */
    public EstimatorEvent(Object src) {
        setSource(src);
    }
    
    /** 
     * Set the source of the event
     * @param src the source of the event
     */
    public void setSource(Object src) {
        source=src;
    }
    
    /**
     * Get the source of the event
     * @return the source of the event
     */
    public Object getSource() {
        return source;
    }
    
}
